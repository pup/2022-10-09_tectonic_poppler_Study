#![allow(non_snake_case)]
fn main() 
{
    let latex = 
    r#"
    \documentclass[preview]{standalone}
    \usepackage{graphicx} 
    \usepackage{environ} 
     
    \NewEnviron{HUGE}{% 
        \scalebox{75}{\BODY}%Goes to 75 from 1 
    } 
     
    \begin{document} 
    
    \HUGE{$y=\left\{x<-1:1,-1\le x<2:x^{2},x\ge2:6-x\right\}$}
     
    \end{document}
    "#;
    let pdf_data: glib::Bytes = glib::Bytes::from(&tectonic::latex_to_pdf(latex).expect("processing failed"));
    let pdf_document = poppler::Document::from_bytes(&pdf_data, None).unwrap();
    for page_index in 0..pdf_document.n_pages()
    {
        let page = pdf_document.page(page_index).unwrap();
        let image_surface = cairo::ImageSurface::create(cairo::Format::ARgb32, page.size().0 as i32, page.size().1 as i32).unwrap();
        let context = cairo::Context::new(&image_surface).unwrap();
        page.render(&context);
        let mut file = std::fs::File::create(format!("image_{}.png", page_index)).unwrap();
        image_surface.write_to_png(&mut file).unwrap();
    }
}
